import random
import time


class PlayMode:
    def __init__(self, max_depth=3, random_moves_order=False):
        self.player = None
        self.max_depth = max_depth
        self.random_moves_order = random_moves_order

        if self.max_depth is None:
            self.max_depth = float('inf')

    def set_player(self, player):
        self.player = player

    def set_max_depth(self, max_depth):
        self.max_depth = max_depth

    def call(self, board, depth=3):
        raise NotImplementedError


class Random(PlayMode):
    name = "Random"

    def call(self, board, depth=3):
        return random.choice(board.legal_moves()), 0


class Minimax(PlayMode):
    name = "MinMax"

    def call(self, board, depth=None):
        if depth is None:
            depth = self.max_depth

        best_score = float('-inf')
        best_move = None
        moves = board.legal_moves()
        if self.random_moves_order:
            random.shuffle(moves)

        for move in moves:
            board.push(move)
            score = self._minimax_min(board, depth - 1)
            # print(f"Move {move} have a score of: {score}")

            board.pop()
            if score > best_score:
                best_score = score
                best_move = move
        return best_move, score

    def _minimax_max(self, board, depth):
        if depth == 0 or board.is_game_over():
            return board.evaluate(board, self.player.color)

        max_eval = float('-inf')
        moves = board.legal_moves()
        if self.random_moves_order:
            random.shuffle(moves)
        for move in moves:
            board.push(move)
            eval = self._minimax_min(board, depth - 1)
            board.pop()
            max_eval = max(max_eval, eval)
        return max_eval

    def _minimax_min(self, board, depth):
        if depth == 0 or board.is_game_over():
            return board.evaluate(board, self.player.color)

        min_eval = float('inf')
        moves = board.legal_moves()
        if self.random_moves_order:
            random.shuffle(moves)
        for move in moves:
            board.push(move)
            eval = self._minimax_max(board, depth - 1)
            board.pop()
            min_eval = min(min_eval, eval)
        return min_eval


class AlphaBeta(PlayMode):
    name = "AlphaBeta"

    def call(self, board, depth=None):
        if depth is None:
            depth = self.max_depth

        alpha = float("-inf")
        beta = float("inf")
        best_move = None
        moves = board.legal_moves()
        if self.random_moves_order:
            random.shuffle(moves)
        for move in moves:
            board.push(move)
            move_value = self._alpha_beta_min(board, alpha, beta, depth - 1)
            # print(f"Move: {move}, Value: {move_value}")
            board.pop()
            if move_value > alpha:
                alpha = move_value
                best_move = move
        return best_move, alpha

    def _alpha_beta_max(self, board, alpha, beta, depth):
        if depth == 0 or board.is_game_over():
            return board.evaluate(board, self.player.color)

        move_value = float('-inf')
        moves = board.legal_moves()
        if self.random_moves_order:
            random.shuffle(moves)
        for move in moves:
            board.push(move)
            value = self._alpha_beta_min(board, alpha, beta, depth - 1)
            # print(f"Move: {move}, Value: {move_value} at depth: {depth}")
            move_value = max(move_value, value)
            board.pop()
            if move_value >= beta:
                break
            alpha = max(alpha, move_value)
        return move_value

    def _alpha_beta_min(self, board, alpha, beta, depth):
        if depth == 0 or board.is_game_over():
            return board.evaluate(board, self.player.color)
        move_value = float('inf')
        moves = board.legal_moves()
        if self.random_moves_order:
            random.shuffle(moves)
        for move in moves:
            board.push(move)
            value = self._alpha_beta_max(board, alpha, beta, depth - 1)
            # print(f"Move: {move}, Value: {move_value} at depth: {depth}")
            move_value = min(move_value, value)
            board.pop()
            if move_value <= alpha:
                break
            beta = min(beta, move_value)
        return move_value


class IterativeDeepening(PlayMode):
    name = "Iterative Deepening"

    def __init__(self, mode, time_limit=10, verbose=False):
        self.mode = mode
        self.time_limit = time_limit
        self.name += " " + self.mode.name
        self.verbose = verbose

    def set_player(self, player):
        self.player = player
        self.mode.set_player(player)

    def set_max_depth(self, max_depth):
        self.max_depth = max_depth
        self.mode.set_max_depth(max_depth)

    def call(self, board):
        best_move = None
        start_time = time.time()
        last_duration = 0
        cumulative_time = 0
        depth = 0
        growth_factor = 2

        while True:
            time_elapsed = time.time() - start_time
            time_remaining = self.time_limit - time_elapsed
            estimated_next_duration = last_duration * growth_factor

            if self.verbose:
                print(
                    f"D-{depth}: Last exploration duration={last_duration}sec, time remaining={time_remaining}sec, Adjusted growth={growth_factor}")

            if self.time_limit and estimated_next_duration > time_remaining:
                if self.verbose:
                    print("-" * 50)
                    print(
                        f"Time elapsed since start={time_elapsed}sec, timeout={self.time_limit}sec, Depth reached={depth}")
                    print(
                        f"Not enough time for another exploration. Estimated time for next exploration={estimated_next_duration}sec, stopping the explorations.")
                break

            depth += 1
            iteration_start = time.time()
            best_move = self.mode.call(board, depth=depth)
            current_duration = time.time() - iteration_start
            cumulative_time += current_duration

            if last_duration > 0:
                growth_factor = (current_duration / last_duration)

            last_duration = current_duration
            if self.time_limit and cumulative_time > self.time_limit:
                if self.verbose:
                    print(
                        f"Exceeded time limit. Stopping at depth {depth}/{self.mode.max_depth} in time: {cumulative_time} seconds")
                break

        return best_move
