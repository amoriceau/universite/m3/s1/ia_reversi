import time

import Heuristics
import Player
import Reversi


class Game:
    def __init__(self, player1=None, player2=None, heuristic=Heuristics.CountingPiece(), board_size=8, verbose=False):
        # Si les joueurs ne sont pas spécifiés, créer deux IA avec un comportement aléatoire
        if player1 is None:
            player1 = Player.AI(Reversi.BLACK)
        if player2 is None:
            player2 = Player.AI(Reversi.WHITE)

        self.players = [player1, player2]
        self.current_player = 0
        self.board = self.create_board(board_size)
        self.heuristic = heuristic
        self.verbose = verbose
        self.board.evaluate = self.heuristic.evaluate.__get__(self.board, Reversi.Board)

    def create_board(self, board_size):
        return Reversi.Board(boardsize=board_size)

    def play(self):
        start_time = time.time()
        turn = 0

        while not self.board.is_game_over():
            turn += 1
            if self.verbose:
                print(f"Turn {turn}")
                print(self.board.display())

            move, move_score = self.get_current_player().move(self.board)
            if move is None:
                if self.verbose:
                    print("Stopping the game...")
                return

            self.board.push(move)
            self.current_player = (self.current_player + 1) % len(self.players)

        end_time = time.time()
        if self.verbose:
            print("\nGame is over!\n")
            print(self.board)
            print("\nGame ended after {} turns".format(turn))
            print("Game time: {}".format(end_time - start_time))
            print("Final pieces count: {} : {}".format(self.board.get_player_pieces(self.players[0].color),
                                                       self.board.get_player_pieces(self.players[1].color)))

    def get_current_player(self):
        return self.players[self.current_player]
