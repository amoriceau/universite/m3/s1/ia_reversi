import itertools
import time

import Player
import Reversi
import ipywidgets as widgets
from IPython.display import display
import pandas as pd
from Game import Game


class Analysis:
    def __init__(self, board_size, players, rivals, heuristics, max_depth, batch_size):
        self.last_results = None
        self.board_size = board_size
        self.players = players
        self.rivals = rivals
        self.heuristics = heuristics
        self.max_depth = max_depth
        self.batch_size = batch_size

    def run_batch(self, player_mode, rival_mode, heuristic, depth, game_tracker):
        batch_progress = widgets.IntProgress(value=0, min=0, max=self.batch_size,
                                             description=f"Batch {player_mode.name} vs {rival_mode.name} using {heuristic.name} heuristic (depth={depth})")
        batch_progress.layout.height = '10px'
        batch_progress.style.description_width = '600px'
        batch_progress.layout.width = '100%'
        display(batch_progress)
        wins = 0

        player_mode.set_max_depth(depth)
        rival_mode.set_max_depth(depth)
        player = Player.AI(color=Reversi.BLACK, mode=player_mode)
        rival = Player.AI(color=Reversi.WHITE, mode=rival_mode)
        start_time = time.time()

        for _ in range(self.batch_size):
            game = Game(player1=player, player2=rival, heuristic=heuristic, board_size=self.board_size, verbose=False)
            game.play()
            winner = game.board.get_result()
            if winner == player.color:
                wins += 1
            batch_progress.value += 1
            game_tracker.value += 1
            game_tracker.description = f"Games: {game_tracker.value}/{game_tracker.max}"
        return wins, time.time()-start_time

    def run(self):
        results = []
        matrix_len = len(self.players) * len(self.rivals) * len(self.heuristics)
        total_games = matrix_len * self.max_depth * self.batch_size

        # Progress bars declarations
        depth_progress = widgets.IntProgress(value=0, min=0, max=self.max_depth)
        depth_progress.layout.height = '10px'
        depth_progress.bar_style = "warning"
        depth_progress.style.description_width = '600px'
        depth_progress.layout.width = '100%'

        mode_progress = widgets.IntProgress(value=0, min=0, max=matrix_len)
        mode_progress.layout.height = '10px'
        mode_progress.bar_style = "success"
        mode_progress.style.description_width = '600px'
        mode_progress.layout.width = '100%'

        games_progress = widgets.IntProgress(value=0, min=0, max=total_games, description=f"Games: 0/{total_games}")
        games_progress.layout.height = '10px'
        games_progress.bar_style = "success"
        games_progress.style.description_width = '600px'
        games_progress.layout.width = '100%'

        display(depth_progress)
        display(f"Analysis will perform {matrix_len * self.max_depth} tests")
        display(f"Analysis will play {total_games} games")
        display("-"*100)
        display(games_progress)
        display(mode_progress)

        for depth in range(1, self.max_depth + 1):
            depth_progress.description = f"Running analysis at depth {depth}"
            for player_mode, rival_mode, heuristic in itertools.product(self.players, self.rivals, self.heuristics):
                mode_progress.description = f"{player_mode.name} vs {rival_mode.name} using {heuristic.name}"
                wins, batch_time = self.run_batch(player_mode, rival_mode, heuristic, depth, games_progress)
                win_percentage = (wins / self.batch_size) * 100
                batch_time_average = batch_time / self.batch_size
                results.append({
                    "Player Mode": player_mode.name,
                    "Rival Mode": rival_mode.name,
                    "Heuristic": heuristic.name,
                    "Depth": depth,
                    "Win Percentage": win_percentage,
                    "Batch Time (AVG)": batch_time_average
                })
                mode_progress.value += 1
            depth_progress.value += 1
        self.last_results = results
        return results

    def display_results(self):
        df = pd.DataFrame(self.last_results)
        df.sort_values(by=(['Rival Mode', 'Heuristic']))
        grouped = df.groupby(['Depth', 'Heuristic'])

        # Analysis Results
        print("Analysis Results:")
        print("-" * 100)
        for (depth, heuristic), group in grouped:
            print(f"Depth: {depth}, Heuristic: {heuristic}")
            print(group.drop(['Depth', 'Heuristic'], axis=1).to_string(index=False))
            print("-" * 100)

        # Algorithms Leaderboard by Win rate ----------------------------------------------------------------------------------------------
        grouped = df.groupby(['Player Mode', 'Heuristic', 'Depth'])['Win Percentage'].mean().reset_index()
        pivot_df = grouped.pivot_table(index=['Player Mode', 'Heuristic'],
                                       columns='Depth',
                                       values='Win Percentage')

        pivot_df.columns = [f'Avg Win % Depth {col}' for col in pivot_df.columns]

        global_avg = df.groupby(['Player Mode', 'Heuristic'])['Win Percentage'].mean()
        global_avg.name = 'Global Avg Win %'

        avg_time = df.groupby(['Player Mode', 'Heuristic'])['Batch Time (AVG)'].mean().round(2)
        avg_time = avg_time.apply(lambda x: f"{x} sec")
        avg_time.name = 'Avg Time per Game'

        final_df = pd.concat([global_avg, avg_time, pivot_df], axis=1).reset_index()
        final_df = final_df.sort_values(by='Global Avg Win %', ascending=False)
        print(final_df)

        # Conclusion ----------------------------------------------------------------------------------------------
        print("\nConclusion:")
        print("-"*100)
        for depth in range(1, self.max_depth + 1):
            best_row = final_df.loc[final_df[f'Avg Win % Depth {depth}'].idxmax()]
            print(f"Depth: {depth}")
            print(f"Best Player Mode: {best_row['Player Mode']}, Heuristic: {best_row['Heuristic']}")
            print(f"Win Percentage: {best_row[f'Avg Win % Depth {depth}']}%, Avg Batch Time: {best_row['Avg Time per Game']}")
            print("-"*100)



