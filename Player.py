import random

import PlayMode
import Reversi


class Player:
    def __init__(self, color, helper=None):
        self.color = color
        if helper is not None:
            helper = PlayMode.IterativeDeepening(mode=helper)
        else:
            helper = helper

    def move(self, board):
        """
        Cette méthode doit être implémentée par les sous-classes.
        Elle doit retourner le mouvement choisi par le joueur.
        """
        raise NotImplementedError("La méthode move doit être implémentée dans les sous-classes.")

    def get_color(self):
        return self.color

    def __str__(self):
        if self.color == Reversi.WHITE:
            return 'O'
        elif self.color == Reversi.BLACK:
            return 'X'
        else:
            return '.'


class Human(Player):
    def move(self, board):
        move = None
        moves = [[x, y] for _, x, y in board.legal_moves()]
        tries = 0
        recommended_move = None
        if self.helper is not None:
            recommended_move = self.helper.move(board)
        while move is None:
            try:
                if (tries >= 3):
                    return None
                tries += 1
                input_string = f"{board.display()}\n\nMouvements autorisés: {moves}\n"
                if recommended_move is not None:
                    _,x,y = recommended_move
                    input_string += f"Mouvement recommandé: {x},{y}\n"
                input_string += "Entrez votre mouvement (format: x,y):\n"
                move_input = input(input_string)
                if len(move_input) == 1 and move_input[0] == 'q':
                    print("Force quit")
                    return None
                x, y = map(int, move_input.split(','))

                if [x, y] not in moves:
                    raise ValueError(f"{x},{y} n'est pas un mouvement valide.")
                move = (self.color, x, y)
            except ValueError as e:
                print(e)
        return move


class AI(Player):
    def __init__(self, color, mode=PlayMode.Random(), time_limit=None):
        super().__init__(color)
        self.mode = mode
        self.mode.set_player(self)
        self.time_limit = time_limit

    def move(self, board):
        return self.mode.call(board)
