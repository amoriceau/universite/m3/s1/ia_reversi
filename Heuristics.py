import Reversi


class Heuristic:
    def evaluate(self, board, player):
        raise NotImplementedError


class CountingPiece(Heuristic):
    name = "Counting Piece"

    def evaluate(self, board, player):
        return board.heuristique(player)


class Positional(Heuristic):
    name = "Positional"

    def __init__(self):
        self.position_weights = None

    def generate_weights(self, size):
        weights = [[0 for _ in range(size)] for _ in range(size)]

        for y in range(size):
            for x in range(size):
                if (x == 0 or x == size - 1) and (y == 0 or y == size - 1):
                    weights[y][x] = 100
                elif x == 0 or x == size - 1 or y == 0 or y == size - 1:
                    weights[y][x] = 10
                else:
                    weights[y][x] = 1
        return weights

    def evaluate(self, board, player):
        score = 0
        size = board.get_board_size()
        if (self.position_weights is None):
            self.position_weights = self.generate_weights(board.get_board_size())

        for y in range(size):
            for x in range(size):
                p = board.get_player_at(x, y)
                if p == player:
                    score += self.position_weights[y][x]
                elif p != player and p != Reversi.EMPTY:
                    score -= self.position_weights[y][x]

        return score


class FullEvaluation(Heuristic):
    name = "Full Evaluation"

    def __init__(self, positional_weight=1, piece_count_weight=1):
        self.positional_heuristic = Positional()
        self.positional_weight = positional_weight
        self.piece_count_weight = piece_count_weight

    def evaluate(self, board, player):
        positional_score = self.positional_heuristic.evaluate(board, player)
        piece_count_score = board.heuristique(player)

        weighted_positional_score = positional_score * self.positional_weight
        weighted_piece_count_score = piece_count_score * self.piece_count_weight

        return weighted_positional_score + weighted_piece_count_score


class AutoWeightedFullEvaluation(Heuristic):
    name = "Auto Weighted Full Evaluation"

    def __init__(self, positional_weight=1, piece_count_weight=1):
        self.positional_heuristic = Positional()
        self.positional_weight = positional_weight
        self.piece_count_weight = piece_count_weight

    def evaluate(self, board, player):
        positional_score = self.positional_heuristic.evaluate(board, player)
        piece_count_score = board.heuristique(player)

        if piece_count_score > 3:
            self.positional_weight = 10
            self.piece_count_weight = 1
        else:
            self.positional_weight = 1
            self.piece_count_weight = 10

        weighted_positional_score = positional_score * self.positional_weight
        weighted_piece_count_score = piece_count_score * self.piece_count_weight

        return weighted_positional_score + weighted_piece_count_score
